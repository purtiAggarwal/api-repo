const User = require('../models/user')
const UserRole = require('../models/user-role')
const mongoose = require('mongoose')
require('../db/mongoose')

class AuthController {
     static register = async (req, res) => {
         // create a new user
         const user = new User(req.body)
         const userRoleId = req.body.role_id;
         
         try{
             
             //get user role
             let roleData = await UserRole.findById(userRoleId)
             
             //assign the users role
             user.role = roleData
             // roleData.populate('role').execPopulate()
            //  console.log(user)
             
             //push the user into role
             roleData.newUser.push(user)
             // console.log(roleData)
             await user.save()
            await roleData.save()
    
            res.status(201).send({ 
                status: 201,
                message: 'SuccessFully Registered!',
                data: {
                    user
                }
             })
    
        } catch(error){
            console.log(error)
           res.status(400).send({
               status: 400,
               message: 'Bad Request',
               error: {
                   error: error
               }
           })
       }
    }

    static login = async (req, res) => {
        try{
            const user = await User.findByCredentials(req.body.mobile_number, req.body.password)
            const token = await user.generateAuthToken()
            res.status(200).send({
                status: 200,
                message: 'Successfully Login',
                data: {
                    userData:user, token 
                }
            })
        }catch(error){
            console.log(error)
            res.status(400).send({
                status: 400,
                message: 'Bad Request',
                error: {
                    error:error
                }
            })
        }
    }
}

module.exports = AuthController