const Event = require('../models/event')
const Client = require('../models/client')
const UserRole = require('../models/user-role')
const Guest = require('../models/guest')
const User = require('../models/user')


class EventController {
    
    static client = async (req, res) => {
        const client = new Client(req.body)
     
        // client.event = event
        await client.save()
        res.status(201).send({
            status: 201,
            message: 'Client Created!',
            data: {
                client
            }
        })
    }

    static event = async (req, res) => {

        let event = new Event(req.body)
        const client_id = req.body.client_id

        try {
            let client = await Client.findById(client_id)
            console.log(client)
                client.newEvent = event 
                event.client = client
                
                
                await event.save()
                await client.save()

                
                res.status(201).send({
                    status: 201,
                    message: 'Event Created!',
                    data:{
                        event
                    }
                })
         
        }catch(error){
            console.log(error)
            return res.status(400).send({
                status: 400,
                message: "Something Went Wrong",
                error: {
                     error
                }
            })
        }
    }

    static userRole = async (req, res) => {
        const userRole = ['Vendor', 'Admin', 'Executive'];
        for(let i = 0; i< userRole.length; i++) {
             var userSaved = new UserRole({
                 //save the roles
                 role: userRole[i]
             });
            const savedResponse = await userSaved.save();
            // console.log(savedResponse);
        }
        res.status(200).send({
            status: 200,
            message: 'User Role Created!'
        })
    }

    static eventDetail = async (req, res) => {
        const events = await Event.find({}).populate('client' , `name`)
        res.send({
            status: 200,
            message: 'All Events',
            data: {
                events
            }
        })
    }

    static clientDetail = async (req, res) => {
        const clients = await Client.find({})
        console.log(clients)
        res.send({
            status: 200,
            message: 'Clients List',
            data:{
                clients
            }
        })
    }

    static guestDetail = async (req, res) => {
        // const event = await Event.find({}).populate('client', 'name')
        const guests = await Guest.find({}).populate('event', `event_title`)

        res.send({
            status: 200,
            message: "Guests List",
            data: {
                guests
            }
        })
    }

    static guest = async (req, res) => {

        let guest = new Guest(req.body)
        const event_id = req.body.event_id
        try{
           
            let event = await Event.findById(event_id)

                event.guests = guest 
                    

                guest.events = event
                
                await event.save()
                await guest.save()

            res.status(201).send({
                status: 201,
                message: 'Guest Created!',
                data: {
                    guest
                }
            })
        }catch(error){
            console.log(error)
            return res.status(400).send({
                status: 400,
                message: "Something Went Wrong",
                error: {
                    error: error
                }
            })
        }
    }

    static executiveAssign = async (req, res) => {
        const event_id = req.body.event_id
        const user_id = req.body.user_id
        try{
            const user = await User.findById(user_id)
            const event = await Event.findById(event_id)
            

            event.executive.push(user)
            user.events.push(event)

            

            await event.save()
            await user.save()
            res.status(200).send({
                status:200,
                message: "Assigned"
            })
        }catch(error){
            res.status(400).send({
                status: 400,
                message: error.message
                
            })
        }
    }

    static vendorAssign = async (req, res) => {
        const event_id = req.body.event_id
        const user_id = req.body.user_id
        try{
            const user = await User.findById(user_id)
            const event = await Event.findById(event_id)
            

            event.vendor.push(user)
            user.events = event

            

            await event.save()
            await user.save()
            res.status(200).send({
                status:200,
                message: "Assigned"
            })
        }catch(error){
            res.status(400).send({
                status: 400,
                message: error.message
                
            })
        } 
    }

    static vendorEvent = async (req, res) => {
       
        try {  
                const event = await Event.aggregate([
                    {$unwind: '$vendor'},
                    {
                        $match : {'vendor': req.user._id}
                    }
                ])
                console.log(event)
                console.log(req.user._id)
                res.status(200).send({
                    status: 200,
                    message: 'Event List',
                    event
                })
            }catch(error){
                console.log(error)
                res.status(400).send({
                    status: 400,
                    message: error.message
                })
            }
    }
    
    static executiveEvent = async (req, res) => {
        try {
            const event = await Event.aggregate([
                {$unwind: '$executive'},
                {
                    $match : {'executive': req.user._id}
                }
            ])
            console.log(event)
            console.log(req.user._id)
            res.status(200).send({
                status: 200,
                message: 'Event List',
                event
            })
        } catch (error) {
            console.log(error)
            res.status(400).send({
                status: 400,
                message: error.message
            })
        }
    }

}

module.exports = EventController