const mongoose = require('mongoose')
const validator = require('validator')

const guestSchema = new mongoose.Schema({
    name: {
        type: String,
        require: [true, 'Name is required'],
        trim: true
    },
    email:{
        type: String,
        required: [true, 'Email is required'],
        unique:[true, 'Email already registered'],
        lowercase:true,
        trim: true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Invalid Email')
            }
        }
    },
    age: {
        type: Number,
        default: 0,
        validate(value){
            if(value < 0){
                throw new Error ('Age must be a positive number')
            }
        }
    },
    gender: {
            type: String,
            require: [true, 'Gender is required']
    },
    phone_number: {
        type: Number,
        min: [10, 'Minimun length of phone number is 10'],
        require: [true, 'Phone Number is required']
    },
    events: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event'
    }
},{
    timestamps: true
})

const Guest = mongoose.model('Guest', guestSchema )

module.exports = Guest