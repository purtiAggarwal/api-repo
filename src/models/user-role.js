const mongoose = require('mongoose')

const UserRoleSchema = new mongoose.Schema({
   role : {
       type: String,
       required: true
   },
   newUser: [{
       type: mongoose.Schema.Types.ObjectId,
       ref: 'User'
   }]
},{
    timestamps: true
})

module.exports = mongoose.model('User-role', UserRoleSchema)