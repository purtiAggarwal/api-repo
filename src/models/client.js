const mongoose = require('mongoose')

const clientSchema = mongoose.Schema({
    name:{
        type:String,
        require: true,
        trim: true
    },
    logo: {
        type: Buffer
    },
    newEvents: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event'
    }
},{
    timestamps: true
})

clientSchema.methods.toJSON = function (){
    const user = this
    const userObj = user.toObject()

    delete userObj.logo

    return userObj
    
}


const Client = mongoose.model('Client', clientSchema)

module.exports = Client