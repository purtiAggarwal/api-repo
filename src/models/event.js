const mongoose = require('mongoose')
const validator = require('validator')

const eventSchema = mongoose.Schema({
    event_title : {
        type: String,
        require: true,
        trim: true
    },
    description: {
        type: String,
        require: true,
        trim:true
    },
    date: {
        type: Date,
        required: true,
        trim: true,
    },
    capacity: {
        type: Number,
        require: true,
        trim: true
    },
    client: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client'
    },
    guests: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Guest'
    },
    event_pic:{
        type: Buffer
    },
    executive: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User' 
    }],
    vendor: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
},{
    timestamps: true
})



eventSchema.methods.toJSON = function(){
    const user = this
    const userObj = user.toObject()

    delete userObj.event_pic
    // delete userObj.executive
    // delete userObj.vendor
    delete userObj.clients
    // delete userObj.guests


    return userObj
}


const Event = mongoose.model('Event', eventSchema)

module.exports = Event