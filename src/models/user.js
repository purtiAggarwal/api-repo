const mongoose = require('mongoose')
const validator = require('validator')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const userRegisterSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        
        trim: true
    },
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        unique: true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Email is invalid')
            }
        }
    },
    password: {
        type: String,
        required: true,
        minlength:7,
        trim: true,
        validate(value){
            if(value.toLowerCase().includes('password')){
                throw new Error('Password cannot contain "Password"')
            }
        }
    },
    mobile_number: {
        type: Number,
        required: [true, 'Mobile number is required'],
        min:[10, 'Min length is 10'],
        // max: [12, 'Max length is 12'],
        unique: true
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    role: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User-role'
    },
    events: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event'
    }]

})

userRegisterSchema.methods.toJSON = function(){
    const user = this
    const userObj = user.toObject()

    delete userObj.password
    delete userObj.tokens

    return userObj
}
userRegisterSchema.methods.generateAuthToken = async function(){
    const user = this
    // console.log("hey")
    const token = jwt.sign({_id:user._id.toString()}, "thisismynewcourse")
    user.tokens = user.tokens.concat({token})
    await user.save()

    return token
}


//hashing the password
userRegisterSchema.pre('save', async function (next) {
    const user = this

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }
    next()
})


userRegisterSchema.statics.findByCredentials = async (mobile_number, password) => {
    const user = await User.findOne( { mobile_number })
    // console.log(user)
    if(!user){
        throw new Error('Unable to login')
    }

    const isMatch = await bcrypt.compare(password, user.password)
    if(!isMatch){
        throw new Error('Unable to login')
    }
    return user
}



const User = mongoose.model('User', userRegisterSchema)

module.exports = User