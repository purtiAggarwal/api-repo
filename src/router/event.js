const express = require('express')
const adminLogin = require('../middleware/admin')
const EventController = require('../controller/eventController')
const vendorLogin = require('../middleware/vendor')
const executiveLogin = require('../middleware/executive')
const multer = require('multer')

const router = express.Router()

const uploadEvent = multer({ dest: 'img/event', limits: { fieldSize: 5 * 1024 * 1024 } });

const uploadClient = multer({ dest: 'img/client', limits: { fieldSize: 5 * 1024 * 1024 } });

router.get('/event_detail', EventController.eventDetail)

router.get('/client_detail', adminLogin, EventController.clientDetail)

router.get('/guest_detail', adminLogin, EventController.guestDetail)

router.get('/vendor_event', vendorLogin, EventController.vendorEvent)

router.get('/executive_event', executiveLogin,  EventController.executiveEvent)

router.post('/client', adminLogin, uploadClient.single('upload'), EventController.client )

router.post('/event', adminLogin, uploadEvent.single('upload'), EventController.event)

router.post('/create-user-role', EventController.userRole)

router.post('/guest',adminLogin, EventController.guest)

router.post('/executive_assign', adminLogin, EventController.executiveAssign)

router.post('/vendor_assign', adminLogin, EventController.vendorAssign)




module.exports = router