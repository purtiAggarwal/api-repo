const express = require('express')
const auth = require('../middleware/auth')
const AuthController = require('../controller/authController')

const router = express.Router()

router.get('/', async (req, res) => {
    // const roleData = await UserRole.find()
    // console.log(roleData) 
    res.send("hey connected!")
})

router.post('/register', AuthController.register)

router.post('/login' , AuthController.login )




module.exports = router