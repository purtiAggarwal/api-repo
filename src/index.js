const express = require('express')
const bodyParser = require('body-parser')
const userRouter = require('./router/user')
const eventRouter = require('./router/event')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.use(userRouter)
app.use(eventRouter)

const port = process.env.PORT || 3000

app.listen(port, () => {
    console.log('Server Up to '+ port)
})