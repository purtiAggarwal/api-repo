const jwt = require('jsonwebtoken')
const User = require('../models/user')
const UserRole = require('../models/user-role')

const executiveLogin = async (req, res, next) => {
    try{
        const token = req.header('Authorization').replace('Bearer ', '')
        const decoded = jwt.verify(token, 'thisismynewcourse')
        const user = await User.findOne({ _id: decoded._id, 'tokens.token': token }).populate('role')
        const userEvent = await User.findOne({ _id: decoded._id, 'tokens.token': token }).populate('events')
        const role = user.role.role
        if(role==="Executive"){
            req.user = user
        }else{
            return res.status(401).send({
                message:'Unautherized Access!',
                status: 401
            })
        }
        next()
        
    }catch(e){
        console.log(e)
        return res.status(401).send({
            message:'Invalid token!',
            status: 401
        })
    }
}

module.exports = executiveLogin